(* Similar to https://github.com/dbuenzli/logs/blob/master/src/logs_cli.ml
   but with a (IMHO: saner) default of INFO and conventionally named
   "--log-level" options. *)

open Cmdliner

let strf = Format.asprintf

let level ?env ?docs () =
  let debug_t =
    let doc =
      "Enable debug logs. Takes precedence over $(b,-l) and $(b,--log-level) \
       options."
    in
    Arg.(value & flag & info [ "debug" ] ~doc ?docs)
  in
  let log_level_t =
    let enum =
      [
        (* The first entry is the default. Aka it is a hack for the option's absent rendering *)
        ("INFO", None);
        (* ("QUIET", Some None); *)
        ("ERROR", Some (Some Logs.Error));
        ("WARNING", Some (Some Logs.Warning));
        ("INFO", Some (Some Logs.Info));
        ("DEBUG", Some (Some Logs.Debug));
      ]
    in
    let log_level = Arg.enum enum in
    let enum_alts = Arg.doc_alts_enum List.(tl enum) in
    let doc =
      strf "Set the verbosity of log messages. $(docv) must be %s." enum_alts
    in
    Arg.(
      value & opt log_level None
      & info [ "log-level" ] ?env ~docv:"LEVEL" ~doc ?docs)
  in
  let quiet_t =
    let doc =
      "Be quiet. Takes precedence over $(b,--debug), $(b,-l) and \
       $(b,--log-level) options."
    in
    Arg.(value & flag & info [ "q"; "quiet" ] ~doc ?docs)
  in
  let choose quiet log_level debug =
    if quiet then None
    else if debug then Some Logs.Debug
    else
      match log_level with
      | Some log_level -> log_level
      | None -> Some Logs.Info
  in
  Term.(const choose $ quiet_t $ log_level_t $ debug_t)
