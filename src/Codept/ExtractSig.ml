open Bos

type scan = DkCoderRuntime | DkCoderProject

let policy =
  let open Fault in
  let open Level in
  let info x = register ~lvl:info x in
  Standard_policies.strict
  (* Less strict for ExtractSig *)
  |> info Standard_faults.unknown_approximated

(** Open for ExtractSig because, especially for scan type DkCoderProject,
    not all transitive dependencies may be available. *)
let open_approximation = true

let precomputed_libs = true
let fail_on_cycles = true

type cmi_pat = {
  pattern : Fpath.t;
  namespace_f : Pat.defs -> Namespaced.t option;
}

type opcode = DirCmi of cmi_pat | ListCmi of Fpath.t list | AllDirs

exception HandledExit

let handle_err msg =
  Logs.err (fun l -> l "FATAL: %a" Fmt.words msg);
  match Logs.level () with Some Debug -> failwith msg | _ -> raise HandledExit

let handle_fpath_err ~msg fpath =
  Logs.err (fun l -> l "FATAL: %s: %a" msg Fpath.pp fpath);
  match Logs.level () with
  | Some Debug -> failwith (Printf.sprintf "%s: %s" msg (Fpath.to_string fpath))
  | _ -> raise HandledExit

let ( let* ) : ('a, 'e) result -> ('a -> ('b, 'e) result) -> ('b, 'e) result =
 fun l r -> match l with Error (`Msg msg) -> handle_err msg | Ok v -> r v

type units_grouping = {
  standard : Unit.u list;
  double_underscored : Unit.u list;
}

let categorize_units (units : Unit.u list) : units_grouping =
  let l =
    List.map
      (fun (unit : Unit.u) ->
        let filepath = Namespaced.filepath unit.path in
        match Astring.String.find_sub ~sub:"__" filepath with
        | None -> `Standard unit
        | Some _ -> `DoubleUnderscored unit)
      units
  in
  let standard =
    List.filter_map (function `Standard u -> Some u | _ -> None) l
  in
  let double_underscored =
    List.filter_map (function `DoubleUnderscored u -> Some u | _ -> None) l
  in
  { standard; double_underscored }

let dir_namespace_f ?except ~var dir defs =
  let all = Astring.String.Map.get var defs in
  let nms () = Namespaced.module_path_of_filename (dir ^ "/" ^ all) in
  match except with
  | None -> Some (nms ())
  | Some (exs, _reason) ->
      if
        List.exists
          (fun ex ->
            let ex = Filename.remove_extension ex in
            String.equal all ex)
          exs
      then None
      else Some (nms ())

(** [load_dir_cmis ?except dir opcodes] load a directory of .cmi files
    except any .cmi that have the same basename as one of the [except]
    exceptions list. *)
let load_dir_cmis ?except dir opcodes =
  DirCmi
    {
      pattern = Fpath.(v dir / "$(basename).cmi");
      namespace_f = dir_namespace_f ?except ~var:"basename" dir;
    }
  :: opcodes

(** Load a list of .cmi files *)
let load_lst_cmis dir cmis opcodes =
  let dir_fp = Fpath.v dir in
  let cmis = List.map (fun cmi -> Fpath.(dir_fp / cmi)) cmis in
  ListCmi cmis :: opcodes

let dkcoder_project_opcodes () = [ AllDirs ]

let dkcoder_runtime_opcodes () =
  []
  |> load_dir_cmis "Stdlib414Shadow"
  |> load_dir_cmis "Tr1Stdlib_V414Extras"
  |> load_dir_cmis "Tr1Stdlib_V414All"
  |> load_dir_cmis "Tr1Stdlib_V414CRuntime"
  |> load_dir_cmis "Tr1Stdlib_V414Io"
  |> load_dir_cmis "Tr1Stdlib_V414Threads"
  |> load_dir_cmis "Tr1Stdlib_V414Gc"
  |> load_dir_cmis "Tr1Stdlib_V414Random"
  |> load_dir_cmis "Tr1Stdlib_V414Unsafe"
  |> load_dir_cmis "Tr1Stdlib_V414Base"
  |> load_dir_cmis "Tr1TosCheck"
  |> load_dir_cmis "angstrom"
  |> load_dir_cmis "Tr1Bogue_Std"
  |> load_dir_cmis "Tr1Bos_Std"
  |> load_dir_cmis "Tr1Clap_Std"
  |> load_dir_cmis "cmdliner"
  |> load_dir_cmis "digestif"
  |> load_dir_cmis "fmt"
  |> load_dir_cmis "fpath"
  |> load_dir_cmis
       ~except:
         ( ["gospel__Symbols"; "gospel__Tast"; "gospel__Tmodule";
            "gospel__Tterm"; "gospel__Tterm_helper"; "gospel__Ttypes";
            "gospel__Uast"; "gospel__Ulexer"],
           "Gospel (gospel/gospel.cmi:)⟶\n\
            Gospel__Symbols (gospel/gospel__Symbols.cmi:)⟶ Gospel" )
       "gospel"
  |> load_dir_cmis "gospel/stdlib"
  |> load_dir_cmis "Tr1Htmlit_Std"
  |> load_dir_cmis "jsonm"
  |> load_dir_cmis "Tr1Logs_Std"
  |> load_dir_cmis "Tr1Logs_Term"
  |> load_dir_cmis "lwt"
  |> load_dir_cmis "lwt/unix"
  |> load_dir_cmis "mtime"
  |> load_dir_cmis "mtime/clock/os"
  |> load_dir_cmis "ocaml-monadic"
  |> load_dir_cmis
        ~except:
        ( ["parsexp__Automaton"; "parsexp__Automaton_intf";
           "parsexp__Parsexp_intf"],
          "Parsexp__ (parsexp/parsexp__.cmi:)⟶\n\
           Parsexp__Automaton (parsexp/parsexp__Automaton.cmi:)⟶ Parsexp__")
        "parsexp"
  |> load_dir_cmis "ptime"
  |> load_dir_cmis "ptime/clock/os"
  |> load_dir_cmis "re"
  |> load_dir_cmis "re/glob"
  |> load_dir_cmis "re/posix"
  |> load_dir_cmis "sedlex"
  |> load_dir_cmis
        ~except:
        ( ["sedlex_ppx__Ppx_sedlex"],
          "Sedlex_ppx (sedlex/ppx/sedlex_ppx.cmi:)⟶\n\
           Sedlex_ppx__Ppx_sedlex (sedlex/ppx/sedlex_ppx__Ppx_sedlex.cmi:)⟶")
        "sedlex/ppx"
  |> load_dir_cmis "sedlex/utils"
  |> load_dir_cmis "semver2"
  |> load_dir_cmis
        ~except:
        ( ["sexplib__Sexp_with_layout"; "sexplib__Type_with_layout"],
          "Sexplib −(sexplib/sexplib.cmi:)⟶
           Sexplib__Sexp_with_layout −(sexplib/sexplib__Sexp_with_layout.cmi:)⟶
           Sexplib__Type_with_layout −(sexplib/sexplib__Type_with_layout.cmi:)⟶")
        "sexplib"
  |> load_dir_cmis "sexplib0"
  |> load_dir_cmis "Tr1Shexp_Std"
  |> load_dir_cmis "tezt"
  |> load_dir_cmis "tezt/core"
  |> load_dir_cmis "tezt/json"
  |> load_dir_cmis "Tr1TinyHttpd_Std"
  |> load_dir_cmis "Tr1TinyHttpd_Prometheus"
  |> load_dir_cmis "tsdl"
  |> load_dir_cmis "uucp"
  |> load_dir_cmis "uuseg"
  |> load_dir_cmis "uuseg/string"
  |> load_dir_cmis "uutf"
  |> load_dir_cmis "xdg"
[@@ocamlformat "disable"]

let pp_string_string_map =
  Astring.String.Map.pp Fmt.(pair ~sep:(any "=") string string)

let single_pat (cmi_pat : cmi_pat) (units : Unit.s Fpath.Map.t) :
    (Unit.s Fpath.Map.t, [ `Msg of string ]) result =
  match OS.Path.query cmi_pat.pattern with
  | Error (`Msg msg) -> handle_err msg
  | Ok cmi_defs_pairs ->
      Logs.debug (fun l ->
          l "pat = @[%a@ ->@ %a@]" Fpath.pp cmi_pat.pattern
            Fmt.(Dump.list (Dump.pair Fpath.pp pp_string_string_map))
            cmi_defs_pairs);
      let r =
        List.fold_left
          (fun units' (cmi, defs) ->
            let filepath = Fpath.to_string cmi in
            match cmi_pat.namespace_f defs with
            | Some namespace ->
                let unit =
                  Unit.read_file policy
                    { format = Cmi; kind = Signature }
                    filepath namespace
                in
                Logs.debug (fun l -> l "single pat unit: %a" Unit.pp_input unit);
                Fpath.Map.add cmi unit units'
            | None ->
                Logs.debug (fun l ->
                    l "single pat unit exception: %a" Fpath.pp cmi);
                units')
          units cmi_defs_pairs
      in
      Ok r

let single_file (cmi_file : Fpath.t) (units : Unit.s Fpath.Map.t) :
    (Unit.s Fpath.Map.t, [ `Msg of string ]) result =
  Logs.debug (fun l -> l "file = @[%a@]" Fpath.pp cmi_file);
  let filepath = Fpath.to_string cmi_file in
  let unit =
    Unit.read_file policy
      { format = Cmi; kind = Signature }
      filepath
      (Namespaced.module_path_of_filename filepath)
  in
  Logs.debug (fun l -> l "single file unit: %a" Unit.pp_input unit);
  Ok (Fpath.Map.add cmi_file unit units)

let run_opcodes opcodes =
  let many op items sigs =
    List.fold_left
      (fun acc item ->
        match acc with
        | Error (`Msg msg) -> handle_err msg
        | Ok sigs' -> op item sigs')
      (Ok sigs) items
  in
  let* cwd = OS.Dir.current () in
  List.fold_left
    (fun acc opcode ->
      match (acc, opcode) with
      | Error (`Msg msg), _ -> handle_err msg
      | Ok sigs, DirCmi pat -> single_pat pat sigs
      | Ok sigs, ListCmi files -> many single_file files sigs
      | Ok sigs, AllDirs ->
          let* childdirs =
            OS.Dir.fold_contents
              ~err:(handle_fpath_err ~msg:"Could not scan directory")
              ~elements:`Dirs ~traverse:`None
              (fun fp acc -> fp :: acc)
              [] cwd
          in
          let childpats =
            List.map
              (fun childdir ->
                let childbasename = Fpath.base childdir in
                {
                  pattern = Fpath.(childbasename / "sig" / "$(basename).cmi");
                  namespace_f =
                    dir_namespace_f ~var:"basename"
                      (Fpath.to_string childbasename);
                })
              childdirs
          in
          many single_pat childpats sigs)
    (Ok Fpath.Map.empty) opcodes

let solve (units_s : Unit.s list) : Unit.u list =
  Logs.debug (fun l ->
      l "units_s = @[%a@]" Fmt.(Dump.list Unit.pp_input) units_s);
  (* Setup Solver modules *)
  let module Envt' = Envt.Core in
  let module Param = struct
    let policy = policy
    let epsilon_dependencies = false
    let transparent_extension_nodes = true
    let transparent_aliases = false
  end in
  let module Eval = Dep_zipper.Make (Envt') (Param) in
  let module S = Solver.Make (Envt') (Param) (Eval) in
  (* Setup Solver values *)
  let namespace = List.map (fun (u : Unit.s) -> u.path) units_s in
  let base_sign = Name.Map.empty in
  let implicits =
    if precomputed_libs then [ ([ "Stdlib" ], Bundle.stdlib) ] else []
  in
  let core =
    Envt.start ~open_approximation ~libs:[] ~namespace ~implicits base_sign
  in
  (* Solve *)
  let solve core (units_s : Unit.s list) =
    let rec solve_harder ancestors state =
      match ancestors with
      | _ :: g :: _ when S.eq state g -> exit 2
      | _ ->
      match S.resolve_dependencies ~learn:true state with
      | Ok (e, l) -> (e, l)
      | Error state ->
          (* We need to run [F.analyze] or indirectly [pp_cycle] to trigger cycle detection. *)
          if fail_on_cycles then (
            let resolver = S.alias_resolver state in
            let format : _ format6 = "Solver failure%s@?@[@<2> @[<0>@;%a@]@]" in
            let pp = Solver.Failure.pp_cycle Eval.block resolver in
            Logs.err (fun l -> l format "." pp state.pending);
            match Logs.level () with
            | Some Debug ->
                let msg = Format.asprintf format "." pp state.pending in
                failwith msg
            | Some _ | None -> exit 7)
          else
            (* Continue solving with approximations *)
            solve_harder (state :: ancestors) @@ S.approx_and_try_harder state
    in
    let _env, units' = solve_harder [] @@ S.start core units_s in
    units'
  in
  solve core units_s

let get_signatures (units : Unit.s Fpath.Map.t) :
    (Module.Namespace.t, [ `Msg of string ]) result =
  (* Solve the units *)
  let (units_s : Unit.s list) = Fpath.Map.bindings units |> List.map snd in
  let (units_u : Unit.u list) = solve units_s in
  Logs.debug (fun l -> l "units_u = @[%a@]" Fmt.(Dump.list Unit.pp) units_u);
  (* Categorize units *)
  let { standard; double_underscored = _ } = categorize_units units_u in
  (* Convert to module signatures *)
  let md (u : Unit.r) =
    let origin = Module.Origin.Unit { source = u.src; path = u.path } in
    Module.Namespace.from_module u.path origin (Unit.signature u)
  in
  let mds =
    List.fold_left
      (fun s mli -> Module.Namespace.merge s (md mli))
      Module.Dict.empty standard
  in
  Ok mds

let run (_ : Copts.t) (scan : scan) (outfile : Fpath.t) (chdir : Fpath.t)
    (pretty : bool) (json : bool) =
  (* Get opcodes *)
  let opcodes =
    match scan with
    | DkCoderRuntime -> dkcoder_runtime_opcodes ()
    | DkCoderProject -> dkcoder_project_opcodes ()
  in
  (* Create .sig.tmp *)
  let outfiletmp = Fpath.to_string outfile ^ ".tmp" |> Fpath.v in
  let sequence () =
    (* Create directories *)
    let parentdir = Fpath.parent outfile in
    let* (_ : bool) = OS.Dir.create parentdir in
    (* Change into [chdir] directory and parse .cmis *)
    let* m2ls_result =
      OS.Dir.with_current chdir (fun () -> run_opcodes opcodes) ()
    in
    let* (units : Unit.s Fpath.Map.t) = m2ls_result in
    (* Run the codept solver *)
    let* (sigs : Module.dict) = get_signatures units in
    (* Write signatures into .sig.tmp *)
    let* write_result =
      OS.File.with_oc outfiletmp
        (fun oc () ->
          let ppf = Format.formatter_of_out_channel oc in
          let pp_schema_ext =
            if json then Schematic.Ext.json else Schematic.Ext.sexp
          in
          let pp_schema = pp_schema_ext Schema.namespace in
          if pretty then Format.fprintf ppf "%a@." pp_schema sigs
          else Schematic.minify ppf "%a@.\n" pp_schema sigs;
          Ok ())
        ()
    in
    let* () = write_result in
    (* Rename .sig.tmp to .sig *)
    OS.Path.move ~force:true outfiletmp outfile
  in
  (* Kick off the sequence *)
  try match sequence () with Error (`Msg msg) -> handle_err msg | Ok () -> ()
  with HandledExit -> exit 1
