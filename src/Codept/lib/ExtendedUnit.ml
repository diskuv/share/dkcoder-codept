(** An extension of [Unit.u] that includes the [ModuleUnit.t].
    That extension allows it to build a dependency graph
    based on [ModuleUnit.t]. *)

type ext2 = {
  signature : Module.signature;
  dependencies : Deps.t;
  module_unit : ModuleUnit.t;
}
[@@deriving show]

type u2 = ext2 UnitPp.t [@@deriving show]

let of_unit_u (u : Unit.u) module_unit =
  {
    u with
    more =
      {
        signature = u.more.signature;
        dependencies = u.more.dependencies;
        module_unit;
      };
  }

let dump_units (units : u2 list) =
  if !CodeptLog.trace_solver then
    CodeptLog.debug (fun l -> l "unit:@ %a" (Fmt.list pp_u2) units)

let calculate_dependency_graph (units : u2 list) : ModuleUnit.t list DepGraph.t
    =
  let module_unit_by_path : ModuleUnit.t Namespaced.map =
    List.fold_left
      (fun graph (u2 : u2) ->
        Namespaced.Map.add u2.path u2.more.module_unit graph)
      Namespaced.Map.empty units
  in
  let dependencies_by_module_id : Namespaced.t list DepGraph.t =
    List.fold_left
      (fun graph (u2 : u2) ->
        DepGraph.add u2.more.module_unit (Deps.paths u2.more.dependencies) graph)
      DepGraph.empty units
  in
  let unique_dependencies_by_module_id : Namespaced.t list DepGraph.t =
    DepGraph.map
      (fun deps -> List.sort_uniq Namespaced.compare deps)
      dependencies_by_module_id
  in
  DepGraph.map
    (fun deps ->
      (* Some dependencies will be to DkCoder Runtime modules like
         [Tr1StdlibV414_Base]. Those are part of [.cma] files and will
         not be present in the ml/mli-based [module_id_by_path]. *)
      List.filter_map
        (fun dep -> Namespaced.Map.find_opt dep module_unit_by_path)
        deps)
    unique_dependencies_by_module_id
