type pkg_source = Pkg.source =
  | Local
  | Unknown
  | Pkg of Namespaced.t
  | Special of string
[@@deriving ord]

type pkg_t = Pkg.t = { source : pkg_source; file : Namespaced.t }
[@@deriving ord]

module UnitId = struct
  type unit_precision = Unit.precision = Exact | Approx [@@deriving ord]
  type m2l_kind = M2l.kind = Structure | Signature [@@deriving ord]

  type t = {
    path : Namespaced.t;
    src : pkg_t;
    kind : m2l_kind;
    precision : unit_precision;
  }
  [@@deriving ord]

  let of_unit (unit : _ Unit.t) =
    {
      path = unit.path;
      src = unit.src;
      kind = unit.kind;
      precision = unit.precision;
    }
end
