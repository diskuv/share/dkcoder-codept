(** Never-shrinking known universe of explicit, optimistic and implicit
    modules. *)

open DkCoder_ModuleCore

type ('a, 'b) t
(** The type of state for the never-shrinking known universe of explicit
    and implicit modules.
    
    ['a] is the type of codept solver state.
    
    ['b] is the type of codept solver environment. *)

type missing_module_response =
  | FoundFiles of CodeptFiles.file list
  | AddedAliases
  | IgnoredSuggestion  (** The type of response to missing modules. *)

(** {1 Constructors} *)

val create :
  initial_env:'b ->
  env_of_state:('a -> 'b) ->
  start_state:('b -> Unit.s list -> 'a) ->
  merge_state:('a -> 'a -> 'a * bool) ->
  organize:
    (CodeptFiles.file list -> ModuleUnit.t Unit.t list Unit.pair * Module.dict) ->
  invariant_check:('a -> 'a -> (unit, [ `Msg of string ]) result) ->
  create_modules_optimistically:
    (ModuleUnit.t Unit.t list ->
    (ModuleUnit.t Unit.t list, [ `Msg of string ]) result) ->
  respond_to_missing_module:
    (path:Namespaced.t ->
    failed_module_id:ModuleId.t ->
    failed_nms:Namespaced.t ->
    failed_loc:Warnings.loc ->
    entry_module_id:ModuleId.t ->
    is_module_known_to_solver:(ModuleId.t -> bool) ->
    (missing_module_response, [ `Msg of string ]) result) ->
  ('a, 'b) t
(** [create ~initial_env ~env_of_state ~start_state ~merge_state ~organize
    ~invariant_check ~create_modules_optimistically ~respond_to_missing_module] creates
    an initial universe.

    [initial_env] is the initial codept environment.

    [env_of_state state] is a function that queries the state to find
    the codept environment.

    [start_state env units_s] is a function that creates an initial state for a
    new units [units_s].

    [merge_state state incremental_state] is a function that merges the
    incremental_state [incremental_state] with the current state [state].
    The return value is a pair [(new_state, changed)] where [changed = true]
    if and only if the merge resulted in a change.

    [organize files] is a function that takes a list of codept files and
    arranges them into either units with M2l structure parsed, or signatures.

    [invariant_check state previous_state] is a function that checks
    whether the new state [state] is correct. The function may access the
    previous state [previous_state] to check if the universe is either
    always growing or stable. The first time the invariant check runs
    the previous state will be the initial state created during {!create}.

    [create_modules_optimistically ~new_units] optimistically creates modules
    as it observes new units [new_units] that are being added to the universe.
    The return value is any newly created optimistic modules.

    [respond_to_missing_module ~on_error ~path ~failed_module_id ~failed_nms
    ~failed_loc ~entry_module_id ~is_module_known_to_solver] provides a
    response to codept when codept detects a missing module.

    The module universe grows but is bound to a single [solve] of a codept
    analysis. *)

(** {1 Search and Growth} *)

val update_state_and_add_codept_units :
  ('a, 'b) t ->
  expand:('a -> 'a option -> unit) ->
  ?new_state:'a ->
  ?new_units:ModuleUnit.t Unit.t list ->
  unit ->
  (unit, [ `Msg of string ]) result
(** [update_state_and_add_codept_units t ~expand ?new_state ?new_units ()]
    incorporates the state [new_state] if specified and adds the codept
    units [new_units] if specified.

    If there were any changes to the known universe, either through
    a modified state or previously unknown units, the [expand] function
    is called.

    The function [expand final_state old_state] is called {b after} units
    are added and {b after} the [state] parameter is incorporated ... if
    and only if one of the following is true:
      
    - one of the added codept [units] was not previously known to the universe [t]
    - the [new_state] parameter was different from what was previously known to the universe [t]

    The [final_state] will be the current state after units and the [new_state]
    parameter have been incorporated, and [old_state] will be some
    old state sufficiently in the past in case you want to check whether
    progress is being made.

    The expectation is that {!add_resolved_units} will be called
    inside [expand state] (although it may come deep in a nested
    expansion chain). *)

val add_resolved_units : ('a, 'b) t -> Unit.u list -> unit
(** [add_resolved_units t units] adds the resolved codept units
    [units] to the known universe [t]. *)

(** {1 Codept Utilities} *)

val codept_resolved_units : ('a, 'b) t -> ExtendedUnit.ext2 Unit.t list
(** [codept_resolved_units t] are the resolved units of the known universe
    [t]. *)

type post_resolve_action =
  | AddMaterial of {
      more_units : ModuleUnit.t Unit.t list Unit.pair;
      more_signatures : Module.dict;
    }
      (** Do a series of {!add_codept_units} for units and {!update_state}
          for signatures.
          Either or both of [more_units] and [more_signatures] will be
          non-empty. *)
  | RetrySolverWithApproximations
      (** Retry the solver [approx_and_try_harder] *)
  | RetrySolverWithModifiedAliases
      (** Retry the solver with the newly updated {!with_module_aliases}.*)

exception UnresolvedModule of string

val resolve_missing_modules :
  ('a, 'b) t ->
  try_quietly:bool ->
  entry_module_id:ModuleId.t ->
  's Solver.Failure.blocker ->
  's Solver.i list ->
  Solver.Failure.alias_resolver ->
  post_resolve_action
(** When there are missing modules, [resolve_missing_modules t block
    pending resolver] can resolve missing modules.
    
    The exception {!UnresolvedModule} is raised when no progress
    can be made solving the missing modules. *)
