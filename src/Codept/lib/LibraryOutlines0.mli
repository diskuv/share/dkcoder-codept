(** Outline of a library before any dependency analysis. *)

open DkCoder_ModuleCore

type t

type open_clause = {
  aliases : M2l.m2l;
      (** Any [module <module> = X__Y__<module>] aliases required before the open. *)
  module_to_open : string;  (** A [open <module_to_open>] statement. *)
}
[@@deriving show]

type outline0 = {
  has_ctrl_module : bool;
  has_open_module : bool;
  opens_for_analysis_f : ModuleId.t -> open_clause list;
      (** [opens_f module_id] returns the list of [open <module>] statements
          and required module aliases that should be applied at the top of
          the module to bootstrap the dependency analysis.

          The order of [opens] is the order the [open <module>] statements
          are applied.

          With codept these are prepended to the module code. *)
}

val of_seq : (string * outline0) Seq.t -> t
val find : string -> t -> outline0
val find_opt : string -> t -> outline0 option
val iter : (string -> outline0 -> unit) -> t -> unit
val bindings : t -> (string * outline0) list
