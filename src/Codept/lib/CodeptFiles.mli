open DkCoder_ModuleCore

type kind =
  | Interface of (Fpath.t * ModuleUnit.t * Namespaced.t)
  | Implementation of (Fpath.t * ModuleUnit.t * Namespaced.t)
  | Signature_file of Fpath.t
  | Signature_content of Module.dict
  | Deferred_implementation of
      (ModuleId.t
      * ModuleUnit.component
      * M2l.m2l
      * Namespaced.t
      * Party.t)

type file = { format : Read.format; kind : kind } [@@deriving show]

val namespace_of_module_id : ModuleId.t -> Namespaced.t

val codept_files_of_unit : ModuleUnit.t -> file list
(** [codept_files_of_unit moduleunit]  *)

val organize :
  Fault.Policy.policy ->
  LibraryOutlines0.t ->
  file list ->
  ModuleUnit.t Unit.t list Unit.pair * Module.dict
(** [organize policy outlines0 files] *)
