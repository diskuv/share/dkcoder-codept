open DkCoder_ModuleCore

val module_id_of_namespaced :
  Namespaced.t -> (ModuleId.t, [ `Msg of string ]) result
(** [module_id_of_namespaced namespace] converts [namespace] to a module id.

    Since we see Namespaced strings like
    [SanetteBogue_Snoke__Tr1Assets] from ["SanetteBogue_Snoke__Tr1Assets.ml"]
    ... not just [SanetteBogue_Snoke.Snoke] from
    ["SanetteBogue_Snoke/Snoke.ml"]
    ... we have to check for interior double-underscores. *)

val is_library : Namespaced.t -> bool

val relativize :
  peer:ModuleId.t -> Namespaced.t -> (ModuleId.t, [ `Msg of string ]) result
(** [relativize ~peer nms] parses the namespaced path [nms] and returns the
    standard module id that is a sibling of the module [module_id] (or
    the standard module id if [nms] is a fully qualified module id).
    
    For example, if [module_id = "DkHello_Std.A.B"] and [nms = "C.D"] then
    the relativization would be [module_id = "DkHello_Std.A.C.D"].

    And if [module_id = "DkHello_Std.A.B"] and [nms = "DkBye_Test__A__B"]
    the relativization would be [module_id = "DkBye_Test.A.B"].
    
    The conventional use is to fully qualify missing modules that are
    referenced in [module_id]. *)
