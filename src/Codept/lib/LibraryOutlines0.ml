open DkCoder_ModuleCore
module M = Map.Make (String)

type open_clause = {
  aliases : M2l.m2l; [@printer M2l.pp]
  module_to_open : string;
}
[@@deriving show]

type outline0 = {
  has_ctrl_module : bool;
  has_open_module : bool;
  opens_for_analysis_f : ModuleId.t -> open_clause list;
}

type t = outline0 M.t

let of_seq = M.of_seq
let iter = M.iter
let find = M.find
let find_opt = M.find_opt
let bindings = M.bindings
