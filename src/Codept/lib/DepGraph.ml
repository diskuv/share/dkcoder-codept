include Map.Make (ModuleUnit)

let pp fmt_value fmt graph =
  Fmt.pf fmt "%a"
    Fmt.(Dump.list (Dump.pair ModuleUnit.pp fmt_value))
    (bindings graph)

let show fmt_value graph = Fmt.str "%a" (pp fmt_value) graph
