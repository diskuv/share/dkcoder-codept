(** A module unit is the combination of the .ml and its optional .mli, or
    any source file that can produce a .ml and/or .mli.
    
    Future versions of module unit will include:
    - ocamllex .mll
    - ocamlyacc .mly
    - reason .re *)

open DkCoder_ModuleCore

type component = {
  abspath : Fpath.t;  (** The absolute path to the component file. *)
  path_relto_scriptdir : Fpath.t;
      (** The relative path from the project root directory. *)
}
[@@deriving show, ord]

type mlonly = { ml_no_mli : component }
type mlmli = { ml : component; mli : component }
type components = Ml of mlonly | MlMli of mlmli
(* | Ocamllex of { mll : component; derived : mlonly }
   | Ocamlyacc of { mly : component; derived : mlmli }
   | Reasonml of { re : component; derived : mlony } *)

type t = {
  module_id : ModuleId.t;
  party : [ `Us | `You | `Them ];  (** Which party owns the module unit. *)
  components : components;
}
[@@deriving show, ord]

val create_ml_only : ModuleId.t -> [ `Us | `You | `Them ] -> ml:component -> t
(** [create_ml_only module_id party ~ml] creates a .ml only unit for the
    module identified by [module_id] owned by the party [party] with
    the .ml located at [ml]. *)

val create_ml_mli :
  ModuleId.t -> [ `Us | `You | `Them ] -> ml:component -> mli:component -> t
(** [create_ml_mli module_id party ~ml ~mli] creates a .ml/.mli unit for the
    module identified by [module_id] owned by the party [party] with
    the .ml located at [ml] and the .mli located at [mli]. *)

val json :
  t ->
  [> `O of
     (string
     * [> `O of
          (string
          * [> `O of
               (string
               * [> `A of [> `String of string ] list | `String of string ])
               list
            | `String of string ])
          list
       | `String of string ])
     list ]
(** [json moduleunit] returns the canonical JSON representation of the
    module unit [moduleunit].
    
    The representation is compatible with Ezjsonm. *)
