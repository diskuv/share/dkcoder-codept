open DkCoder_ModuleCore

type component = { abspath : Fpath.t; path_relto_scriptdir : Fpath.t }
[@@deriving show, ord]

let json_component { abspath; _ } = `String (Fpath.to_string abspath)

type mlonly = { ml_no_mli : component } [@@deriving show, ord]
(** Note: Named [ml_no_mli] so not conflict with other record fields
    [ml] and [mli]. *)

type mlmli = { ml : component; mli : component } [@@deriving show, ord]
type components = Ml of mlonly | MlMli of mlmli [@@deriving show, ord]

type t = {
  module_id : ModuleId.t;
  party : [ `Us | `You | `Them ];
  components : components;
}
[@@deriving show, ord]

let create_ml_only module_id party ~ml =
  { module_id; party; components = Ml { ml_no_mli = ml } }

let create_ml_mli module_id party ~ml ~mli =
  { module_id; party; components = MlMli { ml; mli } }

let json { module_id; party; components; _ } =
  let module_id = ModuleId.json module_id in
  let party =
    match party with
    | `Us -> `String "Us"
    | `You -> `String "You"
    | `Them -> `String "Them"
  in
  let items components =
    `O [ ("id", module_id); ("party", party); ("components", components) ]
  in
  match components with
  | Ml { ml_no_mli } ->
      items (`O [ ("typ", `String "ml"); ("ml", json_component ml_no_mli) ])
  | MlMli { ml; mli } ->
      items
        (`O
          [
            ("typ", `String "mlmli");
            ("ml", json_component ml);
            ("mli", json_component mli);
          ])
