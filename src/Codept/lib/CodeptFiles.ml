open DkCoder_ModuleCore

let pp_dict fmt v =
  Format.fprintf fmt "%a"
    Format.(
      pp_print_list
        ~pp_sep:(fun fmt _v -> fprintf fmt ";@ ")
        (fun fmt v -> fprintf fmt "%s=%a" (fst v) Module.pp (snd v)))
    (Name.Map.bindings v)

type read_format = Read.format = Src | M2l | Parsetree | Cmi [@@deriving show]

type kind =
  | Interface of (Fpath.t * ModuleUnit.t * Namespaced.t)
  | Implementation of (Fpath.t * ModuleUnit.t * Namespaced.t)
  | Signature_file of Fpath.t
  | Signature_content of Module.dict [@printer fun fmt -> pp_dict fmt]
  | Deferred_implementation of
      (ModuleId.t * ModuleUnit.component * M2l.m2l * Namespaced.t * Party.t)
      [@printer
        fun fmt (t1, t2, t3, t4, t5) ->
          Format.fprintf fmt "id=%a@ components=%a@ m2l=%a@ nms=%a@ party=%a"
            ModuleId.pp t1 ModuleUnit.pp_component t2 M2l.pp t3 Namespaced.pp t4
            Party.pp t5]
[@@deriving show]

type file = { format : read_format; kind : kind } [@@deriving show]

let namespace_of_module_id (module_id : ModuleId.t) : Namespaced.t =
  match module_id.state with
  | StandardModule { simple_name; namespace; definition = _ } ->
      (* standard module *)
      (* Namespaced.make
         ~nms:(module_unit.module_id.library :: namespace)
         simple_name *)
      ignore simple_name;
      ignore namespace;
      Namespaced.make (ModuleId.show_double_underscore module_id)
  | LibControlModule ->
      (* lib__ *)
      (* Namespaced.make
         ~nms:[ module_unit.module_id.library ]
         ModuleParsing.libcontrol_simple_name *)
      Namespaced.make (ModuleNamesGen.ctrl module_id.library)
  | LibOpenModule ->
      (* open__ *)
      (* Namespaced.make
         ~nms:[ module_unit.module_id.library ]
         ModuleParsing.libopen_simple_name *)
      Namespaced.make (ModuleNamesGen.open_ module_id.library)

let namespace_of_module_component (module_unit : ModuleUnit.t) : Namespaced.t =
  namespace_of_module_id module_unit.module_id

let codept_files_of_unit (moduleunit : ModuleUnit.t) =
  match moduleunit.components with
  | Ml { ml_no_mli } ->
      [
        {
          format = Src;
          kind =
            Implementation
              ( ml_no_mli.abspath,
                moduleunit,
                namespace_of_module_component moduleunit );
        };
      ]
  | MlMli { ml; mli } ->
      let n = namespace_of_module_component moduleunit in
      [
        { format = Src; kind = Interface (mli.abspath, moduleunit, n) };
        { format = Src; kind = Implementation (ml.abspath, moduleunit, n) };
      ]

let sign filename =
  let parse_sig lexbuf =
    try Schematic.Ext.strict Schema.namespace @@ Sparser.main Slex.main lexbuf
    with Sparser.Error -> Error Unknown_format
  in
  let chan = open_in filename in
  let lexbuf = Lexing.from_channel chan in
  let sigs = parse_sig lexbuf in
  close_in chan;
  sigs

type ('a, 'b, 'c) thirds = First of 'a | Second of 'b | Third of 'c

let info_split ~policy = function
  | { kind = Signature_file f; _ } ->
      let f' = Fpath.to_string f in
      let sc = sign f' in
      let sc =
        begin
          match sc with
          | Error e ->
              Standard_faults.schematic_errors policy (f', "sig", e);
              None
          | Ok v -> Some v
        end
      in
      Third sc
  | { kind = Signature_content sc; _ } -> Third (Some sc)
  | { kind = Deferred_implementation di; _ } -> Second di
  | { kind = Implementation (f, module_id, n); format } ->
      let f' = Fpath.to_string f in
      First ({ Read.kind = Structure; format }, f', n, module_id)
  | { kind = Interface (f, module_id, n); format } ->
      let f' = Fpath.to_string f in
      First ({ Read.kind = Signature; format }, f', n, module_id)

let pair_split l =
  let folder (pair : _ Unit.pair) (x : ModuleUnit.t Unit.t) =
    match x.kind with
    | M2l.Structure -> { pair with ml = x :: pair.ml }
    | Signature -> { pair with mli = x :: pair.mli }
  in
  List.fold_left folder { ml = []; mli = [] } l

let split either =
  let rec split either ((t1, t2, t3) as t) = function
    | [] -> t
    | a :: q ->
        q
        |> split either
             (match either a with
             | First a -> (a :: t1, t2, t3)
             | Second a -> (t1, a :: t2, t3)
             | Third a -> (t1, t2, a :: t3))
  in
  split either ([], [], [])

let pre_organize policy (files : file list) =
  let units, deferred, signatures = split (info_split ~policy) files in
  let signatures =
    Module.Namespace.merge_all @@ Option.List'.filter signatures
  in
  (units, deferred, signatures)

let log_conflict policy proj (path, units) =
  Fault.raise policy Standard_faults.local_module_conflict
    (path, List.map proj units)

(** Basic files reading *)
let ( % ) f g x = f @@ g x

let open_within (outlines0 : LibraryOutlines0.t) (unit : ModuleUnit.t Unit.t) =
  (* Unlike ordinary [codept]! We apply [open <Open__>] that can be unique
     to the library. *)
  match LibraryOutlines0.find_opt unit.more.module_id.library outlines0 with
  | None -> unit
  | Some outline0 ->
      let open_clauses = outline0.opens_for_analysis_f unit.more.module_id in
      List.fold_right
        (fun (open_clause : LibraryOutlines0.open_clause)
             (unit : ModuleUnit.t Unit.t) ->
          (* RunLog.debug (fun l ->
              l "Prepending@ %a to@ [%a]" LibraryOutlines0.pp_open_clause
                open_clause ModuleUnit.pp unit.more); *)
          let open_m2l =
            M2l.Build.ghost @@ M2l.Open (Ident [ open_clause.module_to_open ])
          in
          { unit with code = open_clause.aliases @ (open_m2l :: unit.code) })
        open_clauses unit

(* Replaces default [Unit.read_file] *)
let m2l :
    Fault.Policy.t ->
    Read.kind ->
    string ->
    Namespaced.t ->
    ModuleUnit.t ->
    ModuleUnit.t Unit.t =
 fun policy kind filename path module_unit ->
  let code = Read.file_raw kind filename in
  let (precision : Unit.precision), code =
    match code with
    | Ok c -> (Exact, c)
    | Error (Serialized e) ->
        Standard_faults.schematic_errors policy (filename, "m2l", e);
        (Approx, [])
    | Error (Ocaml (Syntax msg)) ->
        Fault.raise policy Standard_faults.syntaxerr msg;
        (Approx, Approx_parser.lower_bound filename)
    | Error (Ocaml (Lexer msg)) ->
        Fault.raise policy Standard_faults.lexerr (!Location.input_name, msg);
        (Approx, Approx_parser.lower_bound filename)
  in
  {
    path;
    kind = kind.kind;
    precision;
    (* src = Pkg.local filename; *)
    src = { source = Local; file = path };
    code;
    more = module_unit;
  }

let load_file policy outlines (info, file, n, module_unit) =
  m2l policy info file n module_unit |> open_within outlines

let load_deferred
    ((module_id, ml, m2l, path, party) :
      ModuleId.t * ModuleUnit.component * M2l.m2l * Namespaced.t * Party.t) :
    ModuleUnit.t Unit.t =
  {
    path;
    kind = Structure;
    precision = Exact;
    src = { source = Unknown; file = path };
    code = m2l;
    more = ModuleUnit.create_ml_only ~ml module_id party;
  }

let organize policy outlines files =
  let units, deferred, signatures = pre_organize policy files in
  let units = List.map (load_file policy outlines) units in
  let units = List.map load_deferred deferred @ units in
  let units, errs = Unit.Group.(split % group) @@ pair_split units in
  List.iter (log_conflict policy @@ fun (u : _ Unit.t) -> u.src) errs;
  (units, signatures)
