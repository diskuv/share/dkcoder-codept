open DkCoder_ModuleCore
module LibraryMap = Map.Make (String)
module CodeptUnitIdMap = Map.Make (CodeptOrd.UnitId)

let ( let* ) = Result.bind

type missing_module_response =
  | FoundFiles of CodeptFiles.file list
  | AddedAliases
  | IgnoredSuggestion

type ('a, 'b) t = {
  mutable codept_units : ModuleUnit.t CodeptUnitIdMap.t;
  mutable resolved_units : Unit.u CodeptUnitIdMap.t;
  initial_ancestor : 'a;
  mutable ancestors : 'a list;
  env_of_state : 'a -> 'b;
  start_state : 'b -> Unit.s list -> 'a;
  merge_state : 'a -> 'a -> 'a * bool;
  organize :
    CodeptFiles.file list -> ModuleUnit.t Unit.t list Unit.pair * Module.dict;
  invariant_check : 'a -> 'a -> (unit, [ `Msg of string ]) result;
  create_modules_optimistically :
    ModuleUnit.t Unit.t list ->
    (ModuleUnit.t Unit.t list, [ `Msg of string ]) result;
  respond_to_missing_module :
    path:Namespaced.t ->
    failed_module_id:ModuleId.t ->
    failed_nms:Namespaced.t ->
    failed_loc:Warnings.loc ->
    entry_module_id:ModuleId.t ->
    is_module_known_to_solver:(ModuleId.t -> bool) ->
    (missing_module_response, [ `Msg of string ]) result;
}

type post_resolve_action =
  | AddMaterial of {
      more_units : ModuleUnit.t Unit.t list Unit.pair;
      more_signatures : Module.dict;
    }
  | RetrySolverWithApproximations
  | RetrySolverWithModifiedAliases

(* DESIGN NOTES

    Why use Deferred_implementation rather than an immediately-wrtten ModuleUnit.t?
    Because during the analysis rounds the full extent of the implicits won't be known.
    It is better for future-proofing design and efficiency to incrementally add
    signatures (ie. Module.t) and then write out the implied code at the end. *)

let add_resolved_unit resolved_units (unit : Unit.u) =
  CodeptUnitIdMap.add (CodeptOrd.UnitId.of_unit unit) unit resolved_units

let add_codept_unit codept_units (unit : ModuleUnit.t Unit.t) =
  CodeptUnitIdMap.add (CodeptOrd.UnitId.of_unit unit) unit.more codept_units

let upcast_unit (unit : _ Unit.t) = { unit with more = () }

let create (type a b) ~(initial_env : b) ~(env_of_state : a -> b)
    ~(start_state : b -> Unit.s list -> a) ~(merge_state : a -> a -> a * bool)
    ~organize ~(invariant_check : a -> a -> (unit, [ `Msg of string ]) result)
    ~(create_modules_optimistically :
       ModuleUnit.t Unit.t list ->
       (ModuleUnit.t Unit.t list, [ `Msg of string ]) result)
    ~(respond_to_missing_module :
       path:Namespaced.t ->
       failed_module_id:ModuleId.t ->
       failed_nms:Namespaced.t ->
       failed_loc:Warnings.loc ->
       entry_module_id:ModuleId.t ->
       is_module_known_to_solver:(ModuleId.t -> bool) ->
       (missing_module_response, [ `Msg of string ]) result) : (a, b) t =
  let initial_state = start_state initial_env [] in
  {
    codept_units = CodeptUnitIdMap.empty;
    resolved_units = CodeptUnitIdMap.empty;
    initial_ancestor = initial_state;
    env_of_state;
    start_state;
    merge_state;
    organize;
    invariant_check;
    ancestors = [];
    create_modules_optimistically;
    respond_to_missing_module;
  }

let codept_state { initial_ancestor; ancestors; _ } =
  match ancestors with [] -> initial_ancestor | hd :: _tl -> hd

let some_old_state t =
  match t.ancestors with
  (* Heuristics: We can do two mutations in one cycle (ex. add_codept_units
     and update_state), we need [| _ :: _ :: g].
     And we can also do Unit.mli and then Unit.ml.

     So use at least four _. *)
  | _ :: _ :: _ :: _ :: g :: _ -> Some g
  | _ -> None

let incorporate_state t ~new_state =
  let previous_state = codept_state t in
  let* () = t.invariant_check new_state previous_state in
  t.ancestors <- new_state :: t.ancestors;
  Ok ()

type 'a change = NoChange | Change of 'a

let update_state_and_add_codept_units t ~expand ?new_state ?new_units () =
  (* Get some old state *)
  let some_old_state = some_old_state t in

  (* Process [state] *)
  let change_of_state =
    match new_state with
    | None -> NoChange
    | Some state' -> (
        let current_state = codept_state t in
        match t.merge_state state' current_state with
        | _, false -> NoChange
        | chg, true -> Change chg)
  in
  let* () =
    match change_of_state with
    | NoChange -> Ok ()
    | Change chg -> incorporate_state t ~new_state:chg
  in
  (* Process [units] *)
  let* change_of_units =
    match new_units with
    | None | Some [] -> Ok NoChange
    | Some units -> begin
        (* Add any optimistic modules *)
        let* optimistic_units = t.create_modules_optimistically units in

        CodeptLog.debug (fun l ->
            l "optimistic units + transitives =@ %a"
              Fmt.(Dump.list (UnitPp.pp ModuleUnit.pp))
              optimistic_units);

        (* Add the full set of units *)
        let units = optimistic_units @ units in
        List.iter
          (fun (unit : ModuleUnit.t Unit.t) ->
            t.codept_units <- add_codept_unit t.codept_units unit)
          units;
        let current_state = codept_state t in
        let incremental_state =
          t.start_state
            (t.env_of_state current_state)
            (List.map upcast_unit units)
        in
        (* Check if any difference between (current_state, incremental_state), and
           if so get the merged new state *)
        match t.merge_state current_state incremental_state with
        | _, false -> Ok NoChange
        | chg, true -> Ok (Change chg)
      end
  in
  let* () =
    match change_of_units with
    | NoChange -> Ok ()
    | Change chg -> incorporate_state t ~new_state:chg
  in
  (* Call [expand] *)
  begin
    match (change_of_state, change_of_units) with
    | NoChange, NoChange -> ()
    | Change chg, NoChange -> expand chg some_old_state
    | NoChange, Change chg -> expand chg some_old_state
    | Change _first_chg, Change chg ->
        (* The second change uses the state from the first change *)
        expand chg some_old_state
  end;
  Ok ()

let add_resolved_units t (units : Unit.u list) =
  List.iter
    (fun (unit : Unit.u) ->
      t.resolved_units <- add_resolved_unit t.resolved_units unit)
    units

let find_module_unit t (unit : _ Unit.t) =
  match
    CodeptUnitIdMap.find_opt (CodeptOrd.UnitId.of_unit unit) t.codept_units
  with
  | Some v -> v
  | None ->
      failwith
        (Fmt.str "The unit %a had no associated module id" UnitPp.Extras.unit_pp
           unit)

let codept_resolved_units t : ExtendedUnit.ext2 Unit.t list =
  let solution : Unit.u list =
    CodeptUnitIdMap.bindings t.resolved_units |> List.map snd
  in
  let unit2 =
    List.map (fun (u : Unit.u) ->
        let module_unit = find_module_unit t u in
        ExtendedUnit.of_unit_u u module_unit)
  in
  unit2 solution

let is_standard_module (path : Namespaced.t) =
  Namespaced.module_name path
  |> Modname.to_string |> ModuleParsing.is_standard_module

let module_is_known_to_solver { codept_units; _ } module_id =
  let all_module_ids =
    CodeptUnitIdMap.bindings codept_units
    |> List.map snd
    |> List.map (fun module_unit -> module_unit.ModuleUnit.module_id)
  in
  if
    List.exists
      (fun module_id' -> ModuleId.compare module_id module_id' = 0)
      all_module_ids
  then begin
    if !CodeptLog.trace_solver then
      CodeptLog.debug (fun l -> l "%a is known to solver" ModuleId.pp module_id);
    true
  end
  else begin
    if !CodeptLog.trace_solver then
      CodeptLog.debug (fun l ->
          l "@[<hov 2>%a is not known to solver.@;The known modules are:@ %a@]"
            ModuleId.pp module_id
            Fmt.(Dump.list ModuleId.pp)
            all_module_ids);
    false
  end

exception UnresolvedModule of string

(** Our strategy could be either
    A: Fetch pessimistically by only fetching if the only analysis errors
      are [Extern] (aka. missing module).
    B: Fetch optimistically by immediately fetching and retriggering the
      solver if there is an [Extern], even if there are other errors.

    Strategy A does not work with (at least) codept 0.12.0. The Solver will
    present cycles when the only true error is an [Extern]. Said another
    way, the Solver may report false errors (which is fine since we want
    "strict" understanding of the module relationships).

    So we use strategy B. *)
let resolve_missing_modules t ~try_quietly ~entry_module_id block pending
    resolver =
  (* [_cmap] is the cycles map *)
  let map, _cmap = Solver.Failure.analyze block resolver pending in
  let on_error typ =
    let format : _ format6 = "%s@?@[@<2> @[<0>@;%a@]@]" in
    let pp = Solver.Failure.pp_cycle block resolver in
    match Logs.level () with
    | Some Debug ->
        let msg = Format.asprintf format typ pp pending in
        CodeptLog.err (fun l -> l format typ pp pending);
        raise (UnresolvedModule msg)
    | Some _ | None ->
        let msg = Format.asprintf format typ pp pending in
        raise (UnresolvedModule msg)
  in
  let ( let* ) r f =
    match r with Ok v -> f v | Error (`Msg msg) -> on_error msg
  in
  (* Find and perhaps download any unknown [Extern] paths. *)
  let responses =
    List.fold_left
      (fun acc (failed_nms, ({ Solver.input; _ }, status_option)) ->
        let* failed_module_id =
          NamespacedId.module_id_of_namespaced failed_nms
        in
        (* TODO: Submit an issue to codept to expose the (or one of the)
           failed locations, or scroll through its [input.code] to
           find one (or more) plausible matches to the missing [Extern]
           module. Erroring on the whole file is kinda gross. *)
        let failed_loc = Location.in_file (Pkg.filename input.src) in
        match !status_option with
        (* Conditions:

           1. [Extern]
               The compilation unit identified by [failed_nms] depends on
               the unknown module identified by [path].
           2. Must be a library module.
              Example: Retrigger on [MyLibrary_Std] but not [Xyz].

            Post-conditions: [opens] has the new library Open__ module. *)
        | Some (Solver.Failure.Extern path) when NamespacedId.is_library path ->
            if try_quietly then acc
            else
              on_error
              @@ Fmt.str
                   "TODO: Real fetch of %a using ThemParty and adding its \
                    signature file"
                   Namespaced.pp path
        (* Conditions:

           1. [Extern]
               The compilation unit identified by [failed_nms] depends on
               the unknown module identified by [path].
           2. Must be a standard module.
              Example: Retrigger on [MyLibrary_Std] but not [Xyz]. *)
        | Some (Solver.Failure.Extern path) when is_standard_module path ->
            let* m =
              t.respond_to_missing_module ~path ~failed_module_id ~failed_nms
                ~failed_loc ~entry_module_id
                ~is_module_known_to_solver:(module_is_known_to_solver t)
            in
            m :: acc
        | Some _ | None -> acc)
      []
      (Namespaced.Map.bindings map)
  in
  (* Summarize the responses and tell next course of action *)
  let all_found_files =
    List.filter_map
      (fun i -> match i with FoundFiles l -> Some l | _ -> None)
      responses
    |> List.flatten
  in
  match all_found_files with
  | [] ->
      let is_any_added_aliases =
        List.exists
          (fun i -> match i with AddedAliases -> true | _ -> false)
          responses
      in
      if is_any_added_aliases then RetrySolverWithModifiedAliases
      else if try_quietly then RetrySolverWithApproximations
      else
        (* strict mode; don't approximate. fail now! *)
        on_error "codept analysis could not solve which modules are in use."
  | files' ->
      let more_units, more_signatures = t.organize files' in
      AddMaterial { more_units; more_signatures }
