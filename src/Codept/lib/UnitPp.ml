type 'a pair = 'a Unit.pair = { ml : 'a; mli : 'a } [@@deriving show]
type precision = Unit.precision = Exact | Approx [@@deriving show]
type m2l_kind = M2l.kind = Structure | Signature [@@deriving show]

type 'ext t = 'ext Unit.t = {
  path : Namespaced.t;  (** module path of the compilation unit *)
  src : Pkg.t;  (** source file of the compilation unit *)
  kind : m2l_kind;
  precision : precision;
  code : M2l.t;
  more : 'ext;
}
[@@deriving show]

type ext = Unit.ext = { signature : Module.signature; dependencies : Deps.t }
[@@deriving show]

type u = ext t [@@deriving show]

module Extras = struct
  (* Unit.pp and Unit.pp_input reset the formatter with @. *)

  let unit_pp fmt v = Fmt.pf fmt "%a" Fmt.lines (Fmt.str "%a" Unit.pp v)

  let unit_pp_input fmt v =
    Fmt.pf fmt "%a" Fmt.lines (Fmt.str "%a" Unit.pp_input v)
end
