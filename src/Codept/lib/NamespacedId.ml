open DkCoder_ModuleCore

let dotted_name (nms : Namespaced.t) =
  match nms with
  | { namespace = []; name } -> begin
      let s = Unitname.filename name in
      let s, suf =
        if Astring.String.is_suffix ~affix:"__" s then
          (String.sub s 0 (String.length s - 2), "__")
        else (s, "")
      in
      let segs = s |> Astring.String.cuts ~sep:"__" in
      Astring.String.concat ~sep:"." segs ^ suf
    end
  | _ -> Namespaced.to_string nms

let module_id_of_namespaced (nms : Namespaced.t) =
  let s = Namespaced.to_string nms in
  let ctrl_affix = ModuleNamesGen.ctrl "" in
  let open_affix = ModuleNamesGen.open_ "" in
  if Astring.String.is_suffix ~affix:ctrl_affix s then
    Ok
      (ModuleId.create_libcontrol
         ~library:(String.sub s 0 (String.length s - String.length ctrl_affix)))
  else if Astring.String.is_suffix ~affix:open_affix s then
    Ok
      (ModuleId.create_libopen
         ~library:(String.sub s 0 (String.length s - String.length open_affix)))
  else
    let dotted_name = dotted_name nms in
    ModuleId.parse_reversibly dotted_name

let is_library (nms : Namespaced.t) =
  match nms with
  | { namespace = []; name } -> begin
      let s = Unitname.filepath name in
      if Astring.String.is_suffix ~affix:"__" s then false
      else ModuleParsing.is_library s
    end
  | _ -> false

let relativize ~(peer : ModuleId.t) (nms : Namespaced.t) =
  match nms with
  | { namespace = []; name } -> begin
      let segs = Unitname.filename name |> Astring.String.cuts ~sep:"__" in
      begin
        match segs with
        | [ single_seg ] -> ModuleId.parse_peer peer single_seg
        | _ ->
            let dotted_name = Astring.String.concat ~sep:"." segs in
            ModuleId.parse_reversibly dotted_name
      end
    end
  | _ -> ModuleId.parse_peer peer (Namespaced.to_string nms)
