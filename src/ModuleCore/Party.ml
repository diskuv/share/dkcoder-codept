type t = [ `You | `Us | `Them ] [@@deriving show, ord]
