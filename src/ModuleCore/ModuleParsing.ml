let is_upper_char = function 'A' .. 'Z' -> true | _ -> false

let is_libraryowner_remaining_char = function
  | 'a' .. 'z' | '0' .. '9' -> true
  | _ -> false

let is_libraryqualifier_remaining_char = function
  | 'A' .. 'Z' | 'a' .. 'z' | '0' .. '9' -> true
  | _ -> false

let is_libraryunit_remaining_char = function
  | 'A' .. 'Z' | 'a' .. 'z' | '0' .. '9' | '_' -> true
  | _ -> false

let is_standard_modulename_remaining_char =
  ModuleAssumptions.coder_module_charset_does_not_have_quote ();
  is_libraryunit_remaining_char

type doubleunderscore_parser =
  | NoDoubleUnderscores
  | HasUnderscore
  | HasDoubleUnderscore

let has_double_underscores s =
  let state = ref NoDoubleUnderscores in
  let exception Exit in
  begin
    try
      String.iter
        (fun c ->
          match (c, !state) with
          | '_', NoDoubleUnderscores -> state := HasUnderscore
          | '_', HasUnderscore ->
              state := HasDoubleUnderscore;
              raise Exit
          | _, _ -> state := NoDoubleUnderscores)
        s
    with Exit -> ()
  end;
  !state = HasDoubleUnderscore

type library_parser =
  | NotLibrary
  | LibraryOwnerFirst
  | LibraryOwnerRemaining
  | LibraryQualifierRemaining
  | LibraryUnitFirst
  | LibraryUnitRemaining

let reserved_suffix = "__"
let is_reserved s = String.ends_with ~suffix:reserved_suffix s

let is_library s =
  let state = ref LibraryOwnerFirst in
  String.iter
    (fun c ->
      match !state with
      | NotLibrary -> ()
      | LibraryOwnerFirst when is_upper_char c -> state := LibraryOwnerRemaining
      | LibraryOwnerFirst -> state := NotLibrary
      | LibraryOwnerRemaining when is_libraryowner_remaining_char c -> ()
      | LibraryOwnerRemaining when is_upper_char c ->
          state := LibraryQualifierRemaining
      | LibraryOwnerRemaining -> state := NotLibrary
      | LibraryQualifierRemaining when is_libraryqualifier_remaining_char c ->
          ()
      | LibraryQualifierRemaining when '_' = c -> state := LibraryUnitFirst
      | LibraryQualifierRemaining -> state := NotLibrary
      | LibraryUnitFirst when is_upper_char c -> state := LibraryUnitRemaining
      | LibraryUnitFirst -> state := NotLibrary
      | LibraryUnitRemaining when is_libraryunit_remaining_char c -> ()
      | LibraryUnitRemaining -> state := NotLibrary)
    s;
  ModuleAssumptions.coder_library_names_cannot_have_double_underscores ();
  (not (is_reserved s))
  && !state = LibraryUnitRemaining
  && not (has_double_underscores s)

type module_parser = NotModule | ModuleOwnerFirst | ModuleOwnerRemaining

let is_standard_module s =
  let state = ref ModuleOwnerFirst in
  String.iter
    (fun c ->
      match !state with
      | NotModule -> ()
      | ModuleOwnerFirst when is_upper_char c -> state := ModuleOwnerRemaining
      | ModuleOwnerFirst -> state := NotModule
      | ModuleOwnerRemaining when is_standard_modulename_remaining_char c -> ()
      | ModuleOwnerRemaining -> state := NotModule)
    s;
  ModuleAssumptions
  .coder_standard_module_segments_cannot_have_double_underscores ();
  (not (is_reserved s))
  && !state = ModuleOwnerRemaining
  && not (has_double_underscores s)

let libcontrol_simple_name = "lib__"
let is_libcontrol_module = String.equal libcontrol_simple_name
let libopen_simple_name = "open__"
let is_libopen_module = String.equal libopen_simple_name
