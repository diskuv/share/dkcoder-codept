(** Module names in OCaml with double underscores are treated specially by Merlin
    and other tools; they are treated as if dots have replaced the double
    underscores.
    No segment of a standard module may have a double underscore. *)
let coder_standard_module_segments_cannot_have_double_underscores () = ()

(** Module names in OCaml with double underscores are treated specially by Merlin
        and other tools; they are treated as if dots have replaced the double
        underscores.
        A library name may not have a double underscore. *)
let coder_library_names_cannot_have_double_underscores () = ()

(** DkCoder module names must not have single quote characters since quote is
    the only character outside of A-Za-z0-9_ that is supported by codept.
    Actually, the [-] character is also supported by codept, but its
    [Support.is_valid_module_char] states that ocamlopt can compile it but
    emits a warning. *)
let coder_module_charset_does_not_have_quote () = ()
