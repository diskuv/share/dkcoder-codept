type t = [ `Them | `Us | `You ]

val pp : Format.formatter -> t -> unit
val show : t -> string
val compare : t -> t -> int
