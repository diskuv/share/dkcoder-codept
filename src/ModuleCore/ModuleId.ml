type implicit = [ `AssetModule | `EntryNameModule | `VersionModule ]
[@@deriving show, ord]

type optimistic = [ `StitchedParentModule ] [@@deriving show, ord]

type definition = Explicit | Implicit of implicit | Optimistic of optimistic
[@@deriving show, ord]

type state =
  | StandardModule of {
      simple_name : string;
      namespace : string list;
      definition : definition;
    }
  | LibControlModule
  | LibOpenModule
[@@deriving show, ord]

type t = { library : string; module_path : string list; state : state }
[@@deriving show, ord]
(** INVARIANT: [module_path] is not ["lib__"] or any other reserved name. *)

let invariant_module_path_is_not_reserved s =
  failwith
    (Printf.sprintf
       "Invariant failed. The module path segment [%s] must not be reserved \
        (contain any segments with trailing double underscores) for a standard \
        module"
       s)

module ForAdvancedUse = struct
  let get_simple_name_and_namespace hd tl =
    let module_path = hd :: tl in
    let simple_name = List.fold_left (fun _acc v -> v) hd tl in
    let all_but_last = List.rev module_path |> List.tl |> List.rev in
    (simple_name, all_but_last)
end

let validate_seg s =
  if ModuleParsing.is_reserved s then invariant_module_path_is_not_reserved s

let create_standard ~library ~module_path =
  List.iter validate_seg module_path;
  match module_path with
  | [] -> raise (Invalid_argument "The module path must not be empty")
  | hd :: tl ->
      let simple_name, namespace =
        ForAdvancedUse.get_simple_name_and_namespace hd tl
      in
      {
        library;
        module_path;
        state = StandardModule { simple_name; namespace; definition = Explicit };
      }

let create_standard_implicit ~library ~module_path ~implicit =
  List.iter validate_seg module_path;
  match module_path with
  | [] -> raise (Invalid_argument "The module path must not be empty")
  | hd :: tl ->
      let simple_name, namespace =
        ForAdvancedUse.get_simple_name_and_namespace hd tl
      in
      {
        library;
        module_path;
        state =
          StandardModule
            { simple_name; namespace; definition = Implicit implicit };
      }

let create_standard_optimistic ~library ~module_path ~optimistic =
  List.iter validate_seg module_path;
  match module_path with
  | [] -> raise (Invalid_argument "The module path must not be empty")
  | hd :: tl ->
      let simple_name, namespace =
        ForAdvancedUse.get_simple_name_and_namespace hd tl
      in
      {
        library;
        module_path;
        state =
          StandardModule
            { simple_name; namespace; definition = Optimistic optimistic };
      }

let create_libcontrol ~library =
  {
    library;
    module_path = [ ModuleParsing.libcontrol_simple_name ];
    state = LibControlModule;
  }

let create_libopen ~library =
  {
    library;
    module_path = [ ModuleParsing.libopen_simple_name ];
    state = LibOpenModule;
  }

let is_standard { state; _ } =
  match state with StandardModule _ -> true | _ -> false

let is_libcontrol { state; _ } =
  match state with LibControlModule -> true | _ -> false

let is_libopen { state; _ } =
  match state with LibOpenModule -> true | _ -> false

let simple_name { state; _ } =
  match state with
  | StandardModule { simple_name; _ } -> simple_name
  | LibControlModule -> ModuleParsing.libcontrol_simple_name
  | LibOpenModule -> ModuleParsing.libopen_simple_name

let namespace { state; _ } =
  match state with
  | StandardModule { namespace; _ } -> namespace
  | LibControlModule -> []
  | LibOpenModule -> []

let pp_any any fmt _v = Format.fprintf fmt "%s" any

let pp fmt { library; module_path; state = _ } =
  Format.fprintf fmt "%s.%a" library
    Format.(pp_print_list ~pp_sep:(pp_any ".") pp_print_string)
    module_path

let show_underscore { library; module_path; state = _ } =
  Format.asprintf "%s_%a" library
    Format.(pp_print_list ~pp_sep:(pp_any "_") pp_print_string)
    module_path

let show_double_underscore { library; module_path; state = _ } =
  Format.asprintf "%s__%a" library
    Format.(pp_print_list ~pp_sep:(pp_any "__") pp_print_string)
    module_path

let show_dot { library; module_path; state = _ } =
  Format.asprintf "%s.%a" library
    Format.(pp_print_list ~pp_sep:(pp_any ".") pp_print_string)
    module_path

let show_dash { library; module_path; state = _ } =
  Format.asprintf "%s-%a" library
    Format.(pp_print_list ~pp_sep:(pp_any "-") pp_print_string)
    module_path

let json id = `String (show_dot id)
let zero ~msg () = Error (`Msg msg)

let validate_library libname =
  if ModuleParsing.is_library libname then Ok ()
  else
    let msg =
      Printf.sprintf "The first segment %s is not a library name." libname
    in
    zero ~msg ()

let validate_standard_module_acc acc modname =
  match acc with
  | Error e -> Error e
  | Ok () ->
      if ModuleParsing.is_standard_module modname then Ok ()
      else
        let msg =
          Printf.sprintf "The segment %s is not a module name." modname
        in
        zero ~msg ()

let parse_standard s =
  let ( let* ) = Result.bind in
  match String.split_on_char '.' s with
  | [] -> zero ~msg:"The module id cannot be empty" ()
  | [ _single ] ->
      let msg =
        Printf.sprintf
          "The module id %s must at least include the library and one module \
           segment"
          s
      in
      begin
        zero ~msg ()
      end
  | library :: module_path ->
      let* () = validate_library library in
      let* () =
        List.fold_left validate_standard_module_acc (Ok ()) module_path
      in
      Ok (create_standard ~library ~module_path)

let parse_peer module_id s =
  let ( let* ) = Result.bind in
  match (module_id.state, String.split_on_char '.' s) with
  | _, [] -> zero ~msg:"The module id cannot be empty" ()
  | StandardModule { namespace; simple_name = _; definition = _ }, module_path
    ->
      let* () =
        List.fold_left validate_standard_module_acc (Ok ()) module_path
      in
      Ok
        (create_standard ~library:module_id.library
           ~module_path:(namespace @ module_path))
  | LibControlModule, module_path ->
      Ok (create_standard ~library:module_id.library ~module_path)
  | LibOpenModule, module_path ->
      Ok (create_standard ~library:module_id.library ~module_path)

let parse_reversibly =
  let ( let* ) = Result.bind in
  fun s ->
    match String.split_on_char '.' s with
    | [] -> zero ~msg:"The module id cannot be empty" ()
    | [ _single ] ->
        let msg =
          Printf.sprintf
            "The module id %s must at least include the library and one module \
             segment"
            s
        in
        begin
          zero ~msg ()
        end
    | [ library; simple_name ]
      when ModuleParsing.is_libopen_module simple_name
           || String.capitalize_ascii ModuleParsing.libopen_simple_name
              = simple_name ->
        let* () = validate_library library in
        Ok (create_libopen ~library)
    | [ library; simple_name ]
      when ModuleParsing.is_libcontrol_module simple_name
           || String.capitalize_ascii ModuleParsing.libcontrol_simple_name
              = simple_name ->
        let* () = validate_library library in
        Ok (create_libcontrol ~library)
    | library :: module_path ->
        let* () = validate_library library in
        let* () =
          List.fold_left validate_standard_module_acc (Ok ()) module_path
        in
        Ok (create_standard ~library ~module_path)
