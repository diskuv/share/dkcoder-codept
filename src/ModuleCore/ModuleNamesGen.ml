(** Module prefixes that are not for user-created DkCoder modules.
    
    Modules with these prefixes are not DkCoder library modules
    (ex. ["DkHelloScript_Std"]) or DkCoder standard modules
    (ex. ["Example001"]) or DkCoder library control modules
    (["lib__.ml"]). But they can form valid OCaml module names
    recognized by both ocamlc and Dune.
    
    And they can also form valid Dune library names (which
    don't allow single quotes). *)

let () = ModuleAssumptions.coder_module_charset_does_not_have_quote ()
let ctrl library_name = library_name ^ "C__"
let open_ library_name = library_name ^ "O__"
let exe library_name = library_name ^ "X__"
