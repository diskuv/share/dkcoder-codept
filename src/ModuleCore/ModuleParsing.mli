(** {2 Conventional Module Names} *)

val is_library : string -> bool
(** [is_library s] is [true] if and only the string [s] matches the
    DkCoder convention for library names.
    
    Example libraries:
    - ["DkHello_Std"]
    
    Example non-libraries:
    - ["DkHello"]
    - ["DkHello_Std__A__B"] *)

val is_standard_module : string -> bool
(** [is_standard_module s] is [true] if and only the string [s] matches the
    DkCoder convention for standard module names.
    
    Example standard modules:
    - ["DkHello"]
    
    Example that are not standard modules:
    - ["DkHello_Std"]
    - ["DkHello_Std__A__B"] *)

(** {2 Reserved Modules} *)

val libcontrol_simple_name : string
(** The reserved module basename [lib__] for a library control module. *)

val libopen_simple_name : string
(** The reserved module basename [open__] for a library open module. *)

val is_libcontrol_module : string -> bool
val is_libopen_module : string -> bool

val is_reserved : string -> bool
(** Whether or not the module name is reserved for a library
    control module, library open module, or for internal DkCoder use. *)
