type implicit = [ `AssetModule | `EntryNameModule | `VersionModule ]
type optimistic = [ `StitchedParentModule ]
type definition = Explicit | Implicit of implicit | Optimistic of optimistic

type state =
  | StandardModule of {
      simple_name : string;  (** The last name in the module path *)
      namespace : string list;  (** All but the last name in the module path  *)
      definition : definition;
    }
  | LibControlModule
      (** The simple name is {!DkSDKCoder_Common.ModuleParsing.libcontrol_simple_name}. *)
  | LibOpenModule
      (** The simple name is {!DkSDKCoder_Common.ModuleParsing.libopen_simple_name}. *)

type t = private {
  library : string;  (** The library the module belongs to. *)
  module_path : string list;
      (** The full name of the possibly-nested module, without the library name.
          If empty list, then the module is a libcontrol module. *)
  state : state;
}
[@@deriving show, ord]

val create_standard : library:string -> module_path:string list -> t
(** [create_standard ~library ~module_path] creates an id for a standard
    module of the library [library] at the module path [module_path].

    The [module_path] must not be empty. *)

val create_standard_implicit :
  library:string -> module_path:string list -> implicit:implicit -> t
(** [create_standard_implict ~library ~module_path ~implicit] creates an
    implicit id for a standard module of the library [library] at the
    module path [module_path].
    
    The [module_path] must not be empty.
    
    Throws {!Invalid_argument} when there are validation problems. *)

val create_standard_optimistic :
  library:string -> module_path:string list -> optimistic:optimistic -> t
(** [create_standard_implict ~library ~module_path ~optimistic] creates an
      optimistic id for a standard module of the library [library] at the
      module path [module_path].
      
      The [module_path] must not be empty.
      
      Throws {!Invalid_argument} when there are validation problems. *)

val create_libcontrol : library:string -> t
(** [create_libcontrol ~library] creates a [lib__]
    module id for the library [library]. *)

val create_libopen : library:string -> t
(** [create_libopen ~library] creates a [open__]
    module id for the library [library]. *)

val json : t -> [> `String of string ]
(** [json module_id] returns the canonical JSON representation if module id
    [module_id].

    The representation is compatible with Ezjsonm. *)

val is_standard : t -> bool
(** [is_standard module_id] is true if and only if the id is for a standard
    module. *)

val is_libcontrol : t -> bool
(** [is_libcontrol] is true if and only if the id is for a
    [lib__] module. *)

val is_libopen : t -> bool
(** [is_libopen] is true if and only if the id is for a
    [open__] module. *)

val simple_name : t -> string
(** [simple_name module_id] is the last name in the module path.
    
    A {!LibControlModule}'s simple name is
    {!DkSDKCoder_Common.ModuleParsing.libcontrol_simple_name}.
    A {!LibOpenModule}'s simple name is
    {!DkSDKCoder_Common.ModuleParsing.libopen_simple_name}. *)

val namespace : t -> string list
(** [namespace module_id] is all but the last name in the module path.

    A {!LibControlModule}'s namespace is empty.
    A {!LibOpenModule}'s simple name is empty. *)

val show_underscore : t -> string
(** [show_underscore module_id] returns a string with the library
    concatenated with an underscore and then concatenated the modules in
    the module id path seperated by underscores.

    If the module is a libcontrol module then the module id path is
    ["<name of library>'"]. *)

val show_double_underscore : t -> string
(** [show_double_underscore module_id] returns a string with the library
    concatenated with a double underscore (__) and then concatenated the modules in
    the module id path seperated by double underscores.

    Mimics the naming of nested sub-modules created for Dune wrapped libraries.

    If the module is a libcontrol module then the module id path is
    ["<name of library>'"]. *)

val show_dot : t -> string
(** [show_dot module_id] returns a string with the library
    concatenated with a dot ([.]) and then concatenated the modules in
    the module id path seperated by dots.

    If the module is a libcontrol module then the module id path is
    ["<name of library>'"]. *)

val show_dash : t -> string
(** [show_dash module_id] returns a string with the library
    concatenated with a dash ([-]) and then concatenated the modules in
    the module id path seperated by dashes.

    If the module is a libcontrol module then the module id path is
    ["<name of library>'"]. *)

val parse_standard : string -> (t, [ `Msg of string ]) result
(** [parse_standard s] parses the string [s] and returns the id for a
    standard module.
    
    A libcontrol module (ex. ["MyLibrary_Std.lib__"]) is not parseable
    with this standard module function. Library control modules should
    never leak into end-user code. Instead, use {!create_libcontrol}
    in the few locations where libcontrols must be processed. *)

val parse_peer : t -> string -> (t, [ `Msg of string ]) result
(** [parse_peer module_id s] parses the string [s] and returns the
    standard module id that is a sibling of the module [module_id].
    
    For example, if [module_id = "DkHello_Std.A.B"] and [s = "C.D"] then
    the standard peer would be [module_id = "DkHello_Std.A.C.D"].
    
    The conventional use is to fully qualify missing modules that are
    referenced in [module_id]. *)

val parse_reversibly : string -> (t, [ `Msg of string ]) result
(** [parse_reversibly s] should be able to invert any path on the filesystem
    that conforms to DkCoder conventions. So any [open__] or [Open__]
    will translate to a control module, etc. *)

module ForAdvancedUse : sig
  val get_simple_name_and_namespace : 'a -> 'a list -> 'a * 'a list
  (** [get_simple_name_and_namespace head_seg remaining_segs] *)
end
