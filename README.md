# DkCoder high-level drivers for codept

Audience: codept maintainer(s)

Purpose: This repository is meant to let the codept maintainer(s) view how DkCoder is using codept. And if codept wants to re-use some or all of the code, I (jonahbeckford@) can upstream it with a liberal open-source license (preferred) or LGPL with a static linking exclusion for Diskuv (yucky but okay).

License: No license is granted except to the audience above and for the purpose above.

How to build: This repository assumes that you have already built the `codept.bundled` and `codept.lib` libraries. The other libraries can come from opam ... they are convenient but not essential.

Starting point: [src/Codept/lib/ModuleUniverse.mli](src/Codept/lib/ModuleUniverse.mli)
